# Research Project:
### Applications of deep learning for news head line generation
_By "Ollie" Zi Feng Zheng (44609620)_

_Advised by Diego Molla_

This repo contains all of my necessary research leading up to the creation of a model that can generate headlines based on news articles. The `/models/` includes all of the saved models that you may want to try out.  